<%@ page import="java.util.List" %>
<%@ page import="DAOs.Article" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %><%--
  Created by IntelliJ IDEA.
  User: may
  Date: 22/05/2018
  Time: 3:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main Page</title>

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <%--<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>

    <%--summernote--%>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <%--css--%>
    <link rel="stylesheet" type="text/css" href="../css/mainPage.css"/>
    <link rel="stylesheet" type="text/css" href="../css/website.css"/>

</head>

<body class="bodyStyle">

<header>
    <%@ include file="navbar.jsp" %>
</header>

<div class="container">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

            <div class="item active">
                <img src="image/image01.png" alt="Technology" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Technology</h3>
                    <p>Technology is connecting the World at a speed of 'speed of 'light' '</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image02.png" alt="Health" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Health</h3>
                    <p>Health is Wealth!</p>
                </div>
            </div>

            <div class="item">
                <img src="image/image03.png" alt="Education" style="width:100%;">
                <div class="carousel-caption">
                    <h3>Education</h3>
                    <p>Learning new things 'FastPace' is like taking an daily dose of Adrenaline!!</p>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="container">
    <table class="tab1">
        <tbody>
        <form action="SearchServlet" method="post">
            <tr>
                <%--<td width="90" align="right">Search</td>--%>
                <td>
                    <input name="command" type="text" class="allInput" value="${command}"/>
                </td>
                <td width="85" align="right"><input type="submit" class="tabSub" value="Search"/></td>
            </tr>
        </form>
        <form action="SearchServlet" method="post">
            <input type="date" name="date" value="${date}">
            <input type="submit">
        </form>
        <form action="SearchServlet" method="post">
            <input type="submit" name="sortForward" value="sortForward">
        </form>
        <form action="SearchServlet" method="post">
            <input type="submit" name="sortBackward" value="sortBackward">
        </form>
        </tbody>
    </table>
</div>


<div class="bigContainer">


    <div class="container container-margin">
        <c:choose>
            <c:when test="${fn:length(articles) gt 0}">

                <c:forEach var="article" items="${articles}">


                    <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

                    <div class="row contentCard02" id="${article.articleId}">
                        <div class="col-md-12">
                            <div class="innerContent">
                                <h5 class="card-title">${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span>
                                    <span>${article.modifiedDateAndTime}</span></p>

                                    <%--only show short content with 200 chars--%>
                                    <%--<hr class="line">--%>
                                <p class="card-text">${fn:substring(article.content, 0, 199)}</p>


                                <button type="button" class="btn btn-danger load" data-toggle="modal"
                                        data-target="#articleModal"
                                        data-username="${article.username}" data-content="${article.content}"
                                        data-title="${article.title}"
                                        data-id="${article.articleId}">
                                    Load Article
                                </button>
                                <%--Chinchien view blog--%>
                                <input type="button" onclick="location.href='HomePage?status=${article.username}';" value="View Blog" />

                            </div>

                                <%--<p class="card-text">${article.content}</p>--%>
                                <%--<a href="#" class="btn btn-primary">Load Full Article</a>--%>




                <%--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#articleModal"   >--%>
                <%--&lt;%&ndash;data-content="${article.content}" data-title="${article.title}"    &ndash;%&gt;--%>
                <%--Load Article--%>
                <%--</button>--%>
        </div>
        <%--<button id = "load" type="button" class="btn btn-danger load" data-toggle="modal" data-target="#articleModal"--%>
                <%--data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"--%>
                <%--data-id="${article.articleId}">--%>
            <%--Load Article--%>
        <%--</button>--%>

                        <c:choose>
                            <c:when test="${username == article.username}">
                                <button type="button" id="deleteButton" class="btn btn-danger"
                                        onclick="deleteArticle(${article.articleId})"
                                        data-articleID="">Delete Article
                                </button>
                                <button type="button" class="btn btn-danger load" data-toggle="modal"
                                        data-target="#editModal"
                                        data-username="${article.username}" data-content="${article.content}"
                                        data-title="${article.title}"
                                        data-id="${article.articleId}">
                                    Edit Article
                                </button>

                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>

                </c:forEach>
            </c:when>
            <c:otherwise>
                <p>No articles!</p>
            </c:otherwise>
        </c:choose>
        <% String a = "123"; %>
        <br>
    </div>
</div>

<%--<script type="text/javascript" src="../js/jquery.min.js"></script>--%>
<%--<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>


<%@include file="articleModal.jsp" %>
<%@include file="scripts.jsp" %>
<%--<%@include file="editmodal.jsp"%>--%>

<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300,
            focus: true
        });
        // $('.dropdown-toggle').dropdown();
    });
</script>
</body>
</html>
