<%@ page import="DAOs.User" %><%--
  Description: a home page for each user
  ----------------------------------------------------------
  Version  |   Date        |   Created by          |   Description
  v1       |   22/05/2018  |   Chinchien & Massie  |
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HomePage</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

    <%--summernote--%>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <%--css--%>
    <link rel="stylesheet" type="text/css" href="../css/homePage.css"/>
    <link rel="stylesheet" type="text/css" href="../css/website.css"/>
</head>
<body class="bodyStyle">
<header>
    <%@ include file="navbar.jsp" %>
</header>
<div class="container">
    <div class="row">
        <%--display owner Info--%>
        <div class="col-md-4 informationColumn">
            <div class="col-md-12 contentCard">
                <c:choose>
                    <c:when test="${ownerInfo.avatar==null}">
                        <img id="avatar" src="../image/avatar_default.png" style="width: 200px">
                    </c:when>
                    <c:otherwise>
                        <img id="avatar" src="../image/${ownerInfo.avatar}" style="width: 200px">
                    </c:otherwise>
                </c:choose>
                <div class="userInfor">
                    <div class="userInforRow">
                        <p class="userInforTitle">UserName :</p>
                        <p class="userInforDetail">${ownerInfo.username}<c:if
                                test="${ownerInfo.username == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Email :</p>
                        <p class="userInforDetail">${ownerInfo.email}<c:if
                                test="${ownerInfo.email == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Birthday :</p>
                        <p class="userInforDetail">${ownerInfo.dob}<c:if test="${ownerInfo.dob == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Country :</p>
                        <p class="userInforDetail">${ownerInfo.country}<c:if
                                test="${ownerInfo.country == null}">none</c:if></p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Description :</p>
                        <p class="userInforDetail" style="text-align: justify">${ownerInfo.descrp}<c:if
                                test="${ownerInfo.descrp == null}">none</c:if></p>
                    </div>
                    <br>
                    <br>
                    <div class="userInforRow">
                        <button id="delete" onclick="deleteAccount()">  Delete Account </button>
                    </div>
                </div>
            </div>
        </div>
        <%--display articles--%>
        <div class="col-md-8 articleColumn">
            <c:choose>
                <c:when test="${fn:length(articles) gt 0}">

                    <c:forEach var="article" items="${articles}">

                        <div class="row contentCard02" id="${article.articleId}">
                            <div class="col-md-12">
                                <h5 class="card-title">${article.title}</h5>
                                <p class="author">By / <span>${article.username}</span></p>
                                    <%--only show short content with 200 chars--%>
                                <hr class="line">
                                <p class="card-text">${fn:substring(article.content, 0, 199)}</p>

                            </div>
                            <button type="button" class="btn btn-danger load" data-toggle="modal" data-target="#articleModal"
                                    data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"
                                    data-id="${article.articleId}" >
                                Load Article
                            </button>

                            <c:choose>
                                <c:when test="${username == article.username}">
                                    <button type="button" id="deleteButton" class="btn btn-danger" onclick="deleteArticle(${article.articleId})"
                                            data-articleID="">Delete Article
                                    </button>
                                    <button type="button" class="btn btn-danger load" data-toggle="modal" data-target="#editModal"
                                            data-username="${article.username}" data-content="${article.content}" data-title="${article.title}"
                                            data-id="${article.articleId}" >
                                        Edit Article
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <p>hello</p>
                                </c:otherwise>
                            </c:choose>

                        </div>

                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <p>No articles!</p>
                </c:otherwise>
            </c:choose>



        </div>
    </div>
</div>

<%@include file="articleModal.jsp"%>
<%@include file="scripts.jsp"%>
<script>
    $(document).ready(function () {
        $('.summernote').summernote({
            height: 300
            // focus: true
        });
        // $('.dropdown-toggle').dropdown();
    });


    function deleteAccount() {




     var confirmDeletion= confirm( "Are you sure you want to delete your accunt?");

        if(confirmDeletion){
            document.cookie = "edit=delete";


            $.ajax({
                type: 'post',
                url: "/UserInfo",

                success: function (resultData) {

                    alert("Deletion Complete")
                }
            });


        }

    }

</script>


</body>
</html>
