package SecurityPack;

import DAOs.Article;
import DAOs.Comment;
import DAOs.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class AuthenticationModule {

    public AuthenticationModule() {
    }

    public boolean check(HttpSession session, Object tocheck){

        String username=(String)session.getAttribute("username");
        boolean isVerified=false;

       if (tocheck instanceof DAOs.Article){
         String articleUsername= ((Article) tocheck).getUsername();

         if (articleUsername.equals(username)){
             isVerified=true;
           }


       }

       else if (tocheck instanceof User){
           String userUserName=((User) tocheck).getUsername();

           if (userUserName.equals(username)){
               isVerified=true;
           }

       }

        return isVerified;
    }




}
